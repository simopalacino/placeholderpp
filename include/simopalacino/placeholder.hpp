/**
 * Author: Simone Palacino <simo.palacino@gmail.com>
 * Date:   2024 May 09
 * Copyright © Simone Palacino
 */

#pragma once

#include <any>
#include <chrono>
#include <functional>
#include <iomanip>
#include <map>
#include <memory>
#include <regex>
#include <stdexcept>
#include <string>
#include <vector>

namespace simopalacino::utils {
std::string escape(const std::string &str);
}
namespace simopalacino::placeholder {

struct IPlaceholder {
  virtual std::string resolve(const std::vector<std::any> &args) const = 0;
  virtual const std::string &getPattern() const = 0;

  virtual ~IPlaceholder() = default;
};

class ArgCountError : public std::runtime_error {
 public:
  explicit ArgCountError(const std::string &msg) : std::runtime_error(msg) {}
  ArgCountError() : std::runtime_error("Wrong number of arguments provided") {}
};

template <typename... Args>
class Placeholder : public IPlaceholder {
 public:
  using FuncType = std::function<std::string(Args...)>;

  Placeholder(std::string p, FuncType r)
      : pattern_(std::move(p)), resolver_(std::move(r)) {}

  const std::string &getPattern() const override { return pattern_; }

  std::string resolve(const std::vector<std::any> &args) const override {
    if (args.size() != sizeof...(Args)) throw ArgCountError();
    return invoke(args, std::index_sequence_for<Args...>{});
  }

 private:
  std::string pattern_;
  FuncType resolver_;

  template <size_t... I>
  std::string invoke(std::vector<std::any> const &args,
                     std::index_sequence<I...>) const {
    return resolver_(std::any_cast<Args>(args[I])...);
  }
};

class SubstitutionError : public std::runtime_error {
 public:
  explicit SubstitutionError(const std::string &msg)
      : std::runtime_error(msg) {}
  SubstitutionError()
      : std::runtime_error("Error executing the replacing of the placeholder") {
  }
};

class PlaceholderManager {
 public:
  typedef std::string(EscapingFnctTp)(const std::string &str);

  void addPlaceholder(const std::shared_ptr<IPlaceholder> &placeholder) {
    placeholders_[placeholder->getPattern()] = placeholder;
  }

  void setEscapingFnct(std::function<EscapingFnctTp> escapingFnct) {
    escapingFnct_ = escapingFnct;
  }

  // @param input The string to be modified.
  // @param args The map with vectors of arguments to pass to the functions of
  // that placeholder.
  // @return std::string The final string with all the placeholders replaced.
  // Exceptions: May throw SubstitutionError to indicate an error condition.
  std::string replacePlaceholders(
      std::string input,
      const std::map<std::string, std::vector<std::any>> &args = {}) {
    for (const auto &itPh : placeholders_)
      replaceEachPh(input, args, itPh.second);
    return input;
  }

 private:
  std::map<std::string, std::shared_ptr<IPlaceholder>> placeholders_;

  // Exceptions: May throw SubstitutionError to indicate an error condition.
  void replaceEachPh(std::string &input,
                     const std::map<std::string, std::vector<std::any>> &args,
                     const std::shared_ptr<IPlaceholder> &ph) {
    static const std::vector<std::any> empty{};

    const std::string &phStr = ph->getPattern();
    std::regex regex(escapingFnct_(phStr));
    auto it = args.find(phStr);
    const std::vector<std::any> &vArgs = it != args.end() ? it->second : empty;
    std::string fmt;

    try {
      fmt = ph->resolve(vArgs);
      try {
        input = std::regex_replace(input, regex, fmt);
      } catch (...) {
        throw SubstitutionError();
      }
    } catch (const ArgCountError &) {
    }
  }

 private:
  std::function<EscapingFnctTp> escapingFnct_{utils::escape};
};

inline std::string getCurrentSimpleDate() {
  auto now = std::chrono::system_clock::now();
  auto in_time_t = std::chrono::system_clock::to_time_t(now);
  std::stringstream ss;
  ss << std::put_time(localtime(&in_time_t), "%Y-%m-%d");
  return ss.str();
}

inline std::string getCurrentIso8601() {
  auto now = std::chrono::system_clock::now();
  auto in_time_t = std::chrono::system_clock::to_time_t(now);
  std::stringstream ss;
  ss << std::put_time(gmtime(&in_time_t), "%FT%TZ");
  return ss.str();
}

}  // namespace simopalacino::placeholder

namespace simopalacino::utils {

inline std::string escape(const std::string &str) {
  std::regex exp("\\{");
  std::string res = std::regex_replace(str, exp, "\\{");
  exp = std::regex("\\}");
  res = std::regex_replace(res, exp, "\\}");
  return res;
}

}  // namespace simopalacino::utils
