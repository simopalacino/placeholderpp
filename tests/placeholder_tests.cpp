/**
 * Author: Simone Palacino <simo.palacino@gmail.com>
 * Date:   2024 May 09
 * Copyright © Simone Palacino
 */

#include <gtest/gtest.h>

#include <chrono>
#include <filesystem>
#include <functional>
#include <memory>
#include <queue>
#include <regex>
#include <sstream>
#include <string>

#include "simopalacino/placeholder.hpp"

using namespace simopalacino::placeholder;

namespace simopalacino {
TEST(PlaceholderTests, Test1) {
  ASSERT_EQ(utils::escape("{{date}}"), std::string{R"(\{\{date\}\})"});
  ASSERT_EQ(utils::escape("{{date.asd}}"), std::string{R"(\{\{date.asd\}\})"});
}

std::chrono::system_clock::time_point dateStringToTimePoint(
    const std::string& dateStr) {
  std::tm tm = {};
  std::istringstream ss(dateStr);
  ss >> std::get_time(&tm, "%Y-%m-%d %H:%M:%S");
  if (ss.fail()) {
    throw std::runtime_error("Failed to parse date string");
  }
  return std::chrono::system_clock::from_time_t(std::mktime(&tm));
}

TEST(PlaceholderTests, Test2) {
  PlaceholderManager pMan;
  using TimePoint = std::chrono::system_clock::time_point;

  pMan.addPlaceholder(std::make_shared<Placeholder<const char*>>(
      "{{date}}", [](std::string timePoint) {
        auto in_time_t = std::chrono::system_clock::to_time_t(
            dateStringToTimePoint(timePoint));
        std::stringstream ss;
        ss << std::put_time(localtime(&in_time_t), "%Y-%m-%d");
        return ss.str();
      }));
  pMan.addPlaceholder(std::make_shared<Placeholder<const char*>>(
      "{{iso8601}}", [](std::string timePoint) {
        auto in_time_t = std::chrono::system_clock::to_time_t(
            dateStringToTimePoint(timePoint));
        std::stringstream ss;
        ss << std::put_time(gmtime(&in_time_t), "%FT%TZ");
        return ss.str();
      }));

  const char* msg = "my time is {{date}}. ISO8601: {{iso8601}}.";
  const std::string& res = pMan.replacePlaceholders(
      msg, {
               {"{{date}}", {"2024-05-08 14:42:00"}},
               {"{{iso8601}}", {"2024-05-08 14:42:00"}},
           });
  ASSERT_EQ(res, std::string{
                     "my time is 2024-05-08. ISO8601: 2024-05-08T13:42:00Z."});
}

// template <typename _Tp>
// const _Tp& returnSame(const _Tp& arg)
// {
//     return arg;
// }

TEST(PlaceholderTests, Test3) {
  PlaceholderManager pMan;

  const int userId = 123;
  pMan.addPlaceholder(std::make_shared<Placeholder<>>(
      "{{userId}}", [&] { return std::to_string(userId); }));

  const char* msg = "userId: {{userId}}.";
  const std::string& res = pMan.replacePlaceholders(msg);
  ASSERT_EQ(res, std::string{"userId: 123."});
}

class Prova {
 public:
  std::string giveMe(unsigned int userId) {
    const char* msg = "msg: {{userId}}";
    std::map<std::string, std::vector<std::any>> args = {
        {"{{userId}}", {userId}},
    };
    return _pMan.replacePlaceholders(msg, args);
  }

  Prova() {
    _pMan.addPlaceholder(std::make_shared<Placeholder<unsigned int>>(
        "{{userId}}",
        [](const unsigned int& userId) { return std::to_string(userId); }));
  }

 private:
  PlaceholderManager _pMan;
};

TEST(PlaceholderTests, Test4) {
  Prova provaObj;

  ASSERT_EQ(provaObj.giveMe(213), std::string{"msg: 213"});
  ASSERT_EQ(provaObj.giveMe(546), std::string{"msg: 546"});
  ASSERT_EQ(provaObj.giveMe(879), std::string{"msg: 879"});
}

TEST(PlaceholderTests, Test5) {
  PlaceholderManager pMan;

  pMan.addPlaceholder(std::make_shared<Placeholder<int, double>>(
      "{{sum}}", [](int x, double y) {
        std::stringstream ss;
        ss << (x + y);
        return ss.str();
      }));

  const char* input = "The sum is {{sum}}.";
  const std::vector<std::any> arguments = {42, 3.14};

  std::map<std::string, std::vector<std::any>> args = {{"{{sum}}", arguments}};

  std::string processed = pMan.replacePlaceholders(input, args);
  ASSERT_EQ(processed, "The sum is 45.14.");
}

TEST(PlaceholderTests, Test6) {
  PlaceholderManager pMan;

  pMan.addPlaceholder(std::make_shared<Placeholder<int, double>>(
      "{{sum}}", [](int x, double y) {
        std::stringstream ss;
        ss << (x + y);
        return ss.str();
      }));

  pMan.addPlaceholder(std::make_shared<Placeholder<std::string>>(
      "{{snapshot_url}}", [](const std::string& url) { return url; }));

  unsigned int id = 73;
  pMan.addPlaceholder(std::make_shared<Placeholder<>>(
      "{{id}}", [&] { return std::to_string(id); }));

  const char* input = "{{id}}: {{sum}}.";

  std::map<std::string, std::vector<std::any>> args = {
      {"{{sum}}", {42, 3.14}},
  };

  std::string processed = pMan.replacePlaceholders(input, args);
  ASSERT_EQ(processed, "73: 45.14.");
  processed = pMan.replacePlaceholders("{{id}}");
  ASSERT_EQ(processed, "73");
  processed = pMan.replacePlaceholders("snap: {{snapshot_url}}", args);
  ASSERT_EQ(processed, "snap: {{snapshot_url}}");
}

TEST(PlaceholderTests, Test7) {
  PlaceholderManager pMan;

  pMan.addPlaceholder(std::make_shared<Placeholder<int, double>>(
      "{{sum}}", [](int x, double y) {
        std::stringstream ss;
        ss << (x + y);
        return ss.str();
      }));

  pMan.addPlaceholder(std::make_shared<Placeholder<std::string>>(
      "{{snapshot_url}}", [](const std::string& url) { return url; }));

  //   Placeholder userIdPh(
  //       "{{userId}}", [](const Person& person) { return
  //       std::to_string(person.id);
  //       });

  unsigned int id = 73;
  pMan.addPlaceholder(std::make_shared<Placeholder<>>(
      "{{id}}", [&] { return std::to_string(id); }));

  const char* input = "{{id}}: {{sum}}.";

  std::map<std::string, std::vector<std::any>> args = {
      {"{{sum}}", {42, 3.14}},
  };

  std::string processed = pMan.replacePlaceholders("snap: {{snapshot_url}}");
  ASSERT_EQ(processed, "snap: {{snapshot_url}}");

  processed = pMan.replacePlaceholders(
      "snap: {{snapshot_url}}",
      {
          {"{{snapshot_url}}", {std::string{"images/img45678954.jpg"}}},
      });
  ASSERT_EQ(processed, "snap: images/img45678954.jpg");
}

static std::string getRandomQuote() {
  return "Be a fan of anything that tries to replace human contact.";
}

struct Person {
  unsigned long id;
  std::string name;
  unsigned long steps;
  unsigned long calories;
};

TEST(PlaceholderTests, Test8) {
  const char* msg =
      R"(Hello {{name}}, great work today! You've taken {{steps}} steps and burned {{calories}} calories. Remember: "{{quote}}")";

  PlaceholderManager pMgr;
  pMgr.addPlaceholder(std::make_shared<Placeholder<Person>>(
      "{{name}}", [](const Person& person) { return person.name; }));
  pMgr.addPlaceholder(std::make_shared<Placeholder<Person>>(
      "{{steps}}",
      [](const Person& person) { return std::to_string(person.steps); }));
  pMgr.addPlaceholder(std::make_shared<Placeholder<Person>>(
      "{{calories}}",
      [](const Person& person) { return std::to_string(person.calories); }));
  pMgr.addPlaceholder(
      std::make_shared<Placeholder<>>("{{quote}}", getRandomQuote));

  Person person{73, "Sheldon", 370, 1072};
  const std::string res =
      pMgr.replacePlaceholders(msg, {
                                        {"{{name}}", {person}},
                                        {"{{steps}}", {person}},
                                        {"{{calories}}", {person}},
                                    });
  ASSERT_EQ(res,
            "Hello Sheldon, great work today! You've taken 370 steps and "
            "burned 1072 calories. Remember: \"Be a fan of anything that tries "
            "to replace human contact.\"");
}

struct Notification {
  std::string title;
  unsigned msgId;
  std::string msg;
};

struct Sender {
  Sender(unsigned address) : address(address) {}
  std::queue<std::string> txtReceveid;
  unsigned address;
  void receive(const std::string& msg) { txtReceveid.push(msg); }
};

struct SomeEvent {
  std::string name;
};
class SimopPhMgr : public PlaceholderManager {
 public:
  SimopPhMgr() {
    addPlaceholder(
        std::make_shared<Placeholder<>>("{{date}}", getCurrentSimpleDate));
    addPlaceholder(
        std::make_shared<Placeholder<>>("{{iso8601}}", getCurrentIso8601));
    addPlaceholder(std::make_shared<Placeholder<SomeEvent>>(
        "{{eventName}}", [](const SomeEvent& event) { return event.name; }));
  }
};
class NotificationService {
 public:
  NotificationService(std::string templateMsg) : templateMsg_(templateMsg) {
    phMgr_->addPlaceholder(std::make_shared<Placeholder<Notification>>(
        "{{title}}", [](const Notification& notf) { return notf.title; }));
    phMgr_->addPlaceholder(std::make_shared<Placeholder<Notification>>(
        "{{userId}}",
        [](const Notification& notf) { return std::to_string(notf.msgId); }));
    phMgr_->addPlaceholder(std::make_shared<Placeholder<Notification>>(
        "{{msg}}", [](const Notification& notf) { return notf.msg; }));
  }

  void send(Sender& sender, Notification notf) {
    std::string msg =
        phMgr_->replacePlaceholders(templateMsg_, {
                                                      {"{{title}}", {notf}},
                                                      {"{{msgId}}", {notf}},
                                                      {"{{msg}}", {notf}},
                                                  });
    sender.receive(msg);
  }

 private:
  std::shared_ptr<SimopPhMgr> phMgr_{std::make_shared<SimopPhMgr>()};
  std::string templateMsg_;
};

class NotificationDispatcher {
 public:
  NotificationDispatcher(const std::vector<NotificationService>& services,
                         const std::vector<Sender>& senders)
      : services_(std::move(services)), senders_(std::move(senders)) {}

  void dispatch(Notification notf) {
    for (auto& it : services_)
      for (auto& itSend : senders_) it.send(itSend, notf);
  }

  void print() {
    for (auto& itSend : senders_) {
      int i = 0;
      while (itSend.txtReceveid.size()) {
        std::cout << i << ") address: " << itSend.address << ": "
                  << itSend.txtReceveid.front() << std::endl;
        itSend.txtReceveid.pop();
        ++i;
      }
    }
  }

 private:
  std::vector<NotificationService> services_;
  std::vector<Sender> senders_;
};

TEST(PlaceholderTests, Test9) {
  NotificationDispatcher disp(
      {
          NotificationService(
              "Hey {{title}}! Here's your daily report.\nSent time: "
              "{{date}}"),
          NotificationService(
              "iso date: {{iso8601}}. Id msg: {{msgId}}. msg: {{msg}}"),
      },
      {
          Sender{100000},
          Sender{234000},
          Sender{567890},
      });

  disp.dispatch({"man", 19, "Today it will rain."});

  disp.print();
  // std::string txt = sender.txtReceveid.front();
  // ASSERT_EQ(txt, std::string{"Hi Simone (19)! Here's you're daily "
  //                            "report.\nSent time: 2024-05-09"});
}

}  // namespace simopalacino
